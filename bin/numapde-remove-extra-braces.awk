# This awk script removes superfluous braces ('{’ and '}') from the input, 
# deemed a BibLaTeX file. That is, the first '{' and the last '}' on any 
# line are always preserved, as well as any '{' or '}' within a '$...$' block.

# Declare a function which removes all but the first occurrence of the needle (string)
# from the haystack (string). Notice the way we need to declare nFields, fields, output
# as local variables to avoid side effects.
function removeAllButFirst(needle, haystack, nFields, fields, output)
{
	# Split the haystack at all occurrences of needle into fields
	nFields = split(haystack, fields, needle)
	# Prepare the output
	output = fields[1]
	if (nFields > 1) { output = output needle }
	for (iterField = 2; iterField <= nFields; iterField++) { output = output fields[iterField] }
	return output
}

# Declare a function which removes all but the last occurrence of the needle (string)
# from the haystack (string). Notice the way we need to declare nFields, fields, output
# as local variables to avoid side effects.
function removeAllButLast(needle, haystack, nFields, fields, output)
{
	# Split the haystack at all occurrences of needle into fields
	nFields = split(haystack, fields, needle)
	# Prepare the output
	output = ""
	for (iterField = 1; iterField < nFields; iterField++) { output = output fields[iterField] }
	if (nFields > 1) { output = output needle }
	output = output fields[nFields] 
	return output
}

# Copy lines verbatim which do not have a '{...}' pair, e.g., lines that are equal to '}'
# or lines such as '@ARTICLE{MaurerZowe:1979:1'
!/.*{.*}/ { print; next }

# From now on, the line has at least one occurrence of '{' and '}' in that order
{
	# Split the line into fields at separator '$'
	nFields = split($0, fields, "$")

	# If we do not have a '$', replace all but the first '{' and all but the last '}' by ''
	# and move to the next line
	if (nFields == 1)
	{
		fields[1] = removeAllButFirst("{", fields[1])
		fields[1] = removeAllButLast("}", fields[1])
		print fields[1]
		next
	}

	# From now on, we have at least one '$' and we assume they come in '$...$' blocks.
	# In the first field (before the first '$'), replace all '}' and all but the first '{' by ''
	gsub("}", "", fields[1])
	printf "%s", removeAllButFirst("{", fields[1])

	# Remove all '{' and '}' from intermediate fields except when inside a '$...$' block
	for (iterField = 2; iterField < nFields; iterField++) 
	{ 
		# If the field number is odd, we are outside a '$...$' block and we remove all '{' and '}'.
		# Otherwise we are inside a '$...$' block and we lave all '{' and '}' intact.
		if (iterField %2 == 1) { gsub("[{}]", "", fields[iterField]) }
		printf "$%s", fields[iterField] 
	}
	# In the last field, replace all '{' and all but the last '}' by ''
	gsub("{", "", fields[nFields])
	printf "$%s", removeAllButLast("}", fields[nFields])
	printf "\n"
}
