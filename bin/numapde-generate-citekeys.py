#!/usr/bin/env python3
# This script generates preliminary BibLaTeX cite keys for all entries in a 
# given BibLaTeX file.bib, using the author and date fields of each entry. 
# The format of the generated keys is
#   AuthorAuthor:YYYY:XXX
# where Author stands for an author's last name(s) and YYYY stands for the year
# from the date field. The XXX marks the cite key as preliminary.
# The file.bib, with the keys replaced by the generated keys, is dumped to 
# stdout.

# Define the person(s) responsible for maintenance
maintainers = ('Andreas Naumann, Roland Herzog')

# Resolve the dependencies
try:
    import pybtex.database as db
except ImportError:
    print('Please install pybtex using\n  pip3 install pybtex\n')
    raise

# Resolve the dependencies
import sys
import argparse
import os.path

# Define some examples for the help text
epilog = r"""

Examples:
    {scriptName} numapde.bib 
    {scriptName} numapde.bib | sponge numapde.bib
    
Maintainers: {maintainers}""".format(scriptName = sys.argv[0], maintainers = maintainers)

# Provide the command line arguments to the parser
parser = argparse.ArgumentParser(description = 'This script generates preliminary cite keys for the given BibLaTeX file, according to the numapde cite key standard. The processed file (with previous cite keys replaced) is written to stdout. Error messages are written to stderr and the exists with non zero exit code.', epilog =  epilog , formatter_class = argparse.RawTextHelpFormatter)
parser.add_argument('bibfile', metavar = 'bibfile', help = 'The BibLaTeX file to be processed, where the special name - represents the standard input.', nargs = '?', default = '-')
args = parser.parse_args()

# Read the contents of the BibLaTeX file into a dictionary (using pybtex)
if args.bibfile == "-":
    fileAsStr = sys.stdin.read()
    bibData   = db.parse_string(fileAsStr, bib_format="bibtex")
    printRepl = False # do not print the association from old to new on the terminal when reading the input from the terminal
else:
    fileAsStr = None
    if os.path.getsize(args.bibfile) == 0:
        print('The given file {} is empty. That looks like an error.'.format(args.bibfile), file=sys.stderr)
        sys.exit(1)
    bibData   = db.parse_file(args.bibfile)
    printRepl = True

# Define all replacements 
# Replace the {} and apostrophes from some encodings
# This list will be expanded on an on-demand basis
replList = {u'{':'', u'}':'', u"'":''} 
# Replace hyphens
replList.update({ u'-':''}) 
# Replace German umlauts and ß
replList.update({ u'ü':'ue', u'Ü':'Ue', u'ä':'ae', u'Ä':'Ae', u'ö':'oe', u'Ö':'Oe', u'ß':'ss'}) 
# Replace Polish letters
replList.update({ u'Ł':'L', u'ł':'l', u'ż':'z'}) 
# Replace Danish and Norwegian letters
replList.update({u'Æ':'Ae', u'Ø':'O', u'Å':'Aa', u'æ':'ae', u'ø':'o', u'å':'a'}) 
# Replace French accented letters
replList.update({u'à':'a', u'é':'e', u'è':'e', u'ô':'o', u'ë':'e'}) 
# Replace more letters
replList.update({u'-':'', u'ı̀':'i', u'ñ':'n', u'á':'a', u'ú':'u', u'ç':'c', u'ã':'a'}) 
# Replace more letters
replList.update({u'ş':'s', u'ó':'o', u'č':'c', u'ı́':'i', u'ž':'z', u'í':'i', u'ţ':'t', u'ř':'r', u'ı̂':'i', u'ć':'c', u'ě':'e'})
# Replace more letters
replList.update({u'ḡ':'g', u'ń':'n', u'ğ':'g', u'ǧ':'g', u'Ō':'O', u'ō':'o', u'Š':'S', u'š':'s', u'Ž':'Z'})
# Replace more letters
replList.update({u'ý':'y', u'ı̆':'i', u'ů':'u', u'ă':'a', u'Jr.':''})

sys.tracebacklimit = None
class MissingField(Exception):
    """ Explains missing bibtex fields"""
    def __init__(self, expl):
        self.expl = expl
        self.message = None

    def setKey(self, key):
        self.message = '{}\nThe malformed entry has key {}'.format(self.expl, key)
    
    def __str__(self):
        return self.message

def createKey(entry, existingKeys, rev = 'XXA'):
    r""" creates a key of the form LastNames:Year:XX[A-Z] where LastNames are the transcribed
    and capitalized last names of the authors and year is the publication year. The suffix 
    XX[A-Z] is a placeholder for the disambiguation number, which is going to be set elsewhere,
    see numapde-finalize_citekeys.sh.
    The last digit in the suffix is in the range A-Z. That allows multiple to generate keys for 
    multiple publications of the same author(s) in one year.
    """
    # Get the authors' last names; if authors field is empty, try editors instead
    try:
        authors = entry.persons['AUTHOR']
    except KeyError:
        try:
            authors = entry.persons['EDITOR']
        except KeyError:
            raise MissingField('ERROR: The author and the editor field are missing.')

    # Transform the authors' last names by replacing non-ASCII characters and 
    # capitalizing each last name (but leaving upper case letters as they are, in order
    # to handle cases like d'Halluin -> DHalluin; capitalize() does not do this, so we define our own
    # cap function
    def cap(word):
        if not word:
            return ""
        else:
            return word[0].upper() + word[1:]
    transNames = []
    for a in authors:
        for n in a.prelast_names:
            n = cap(n)
            for src, dest in replList.items():
                n = n.replace(src,dest)
            transNames.append(cap(n))
        for n in a.last_names:
            n = cap(n)
            for src, dest in replList.items():
                n = n.replace(src,dest)
            transNames.append(cap(n))
    names = "".join(transNames) 

    # Split the date into year-month
    try:
        date = entry.fields['DATE']
        date = date.split('-')
        year = date[0] 
    except KeyError:
        raise MissingField('ERROR: the DATE field is missing.')
        

    # TODO: check date validity 
    key  = "{}:{}:{}".format(names, year, rev)
    # Check whether the key is already in use
    while key in existingKeys:
        if rev[-1] == 'Z':
            raise RuntimeError(f'We have reached the last revision digit Z with the key {key}. Please open an issue.')
        revList = list(rev)
        revList[-1] = chr(ord(revList[-1])+1)
        rev = ''.join(revList)
        key  = "{}:{}:{}".format(names, year, rev)

    try:
        key.encode('ascii')
    except UnicodeEncodeError:
        print('There seems to be a non-ASCII character in the generated key {}.\nUsing ASCII-encoding with name leads to {}.\nThe original key was {}\n'.format(key, key.encode('ascii', 'namereplace'), entry.key), file = sys.stderr)
    return key
    # return key.replace('{','').replace('}','').encode('ascii','replace')
    # return str(key.replace('{','').replace('}','').encode('ascii','namereplace'))

# print("{} got key: {}".format(testEntry.key, createKey(testEntry)))
oldToNew = {}
missingFields = []
newKeys = set()
for key in bibData.entries:
    entry = bibData.entries[key]
    try:
        newKey = createKey(entry, newKeys)
        oldToNew[key] = newKey
        newKeys.add(newKey)
        if printRepl:
            print("The original key {} is being replaced with the temporary key: {}".format(key, oldToNew[key]), file = sys.stderr)
    except MissingField as mf:
        mf.setKey(key)
        missingFields.append(mf)
        
    except KeyError as err:
        print("Did not find the key {} for bibtex key {}".format(str(err), key), file = sys.stderr)

# Overwrite the old keys with the newly generated ones,
# using a simple text replacement instead of the pybtex write
if fileAsStr is None:
    fileAsStr = open(args.bibfile, 'r').read()
for old,new in oldToNew.items():
    fileAsStr = fileAsStr.replace(old, new, 1)

print(fileAsStr)
if len(missingFields) > 0:
    for mf in missingFields:
        print(mf, file = sys.stderr)
    sys.exit(1)
