# This awk script finds author names, which are suspicious in the sense that they have been
# assigned multiple given names (some of which might belong to the family name).
# Confirmed author names will be exempted.
#
# Input variables:
#
#   field   specifies a regular expression used to finding fields to consider for suspicious 
#           author name search
#           Example: " TITLE"
#
# This script takes two file arguments, as in
#   awk -f numapde-find-suspicious-author-names.awk -v "field= AUTHOR" numapde-confirmed-author-names.txt numapde.bib
# The first file argument contains confirmed author names.
# The second file argument is the .bib file to be processed.

BEGIN { 
	# Define the regular expression to find suspicious author names
	# The first regex matches names such as Núñez, José Vidal
	# The second regex matches names such as Chen-song
	SuspiciousNameRegex = ", [^ .,]+ [^ .,]+[ }]|-[a-z]"
}

# If we are reading the file with exceptions, build a regular expression
# https://www.unix.com/shell-programming-and-scripting/197183-awk-delete-word-when-found.html
{
	if (FNR == NR) 
	{
		# Append the current line to the regular expression 
		confirmedAuthorNames = (confirmedAuthorNames?confirmedAuthorNames"|":"") $0 
		next
	}
}

# In case the the field matches, and a suspicious name is determined, print the line
{
	if ($0 ~ field)
	{
		# Remove confirmed author names
		lineWithExceptionsRemoved = $0
		gsub(confirmedAuthorNames, "", lineWithExceptionsRemoved)
		# Split the remaining part of the line at all "and"s
		split(lineWithExceptionsRemoved, array, "and")
		# If any part matches the suspicious author expression, print the line
		for (var in array)
		{
			# print array[var]
			if (array[var] ~ SuspiciousNameRegex) { print array[var]; print $0 }
			next
		}
	}
}

