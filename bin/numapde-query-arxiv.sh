#!/bin/bash
# This script queries https://arxiv2bibtex.org about the arXiv identifier
# given and retrieves a bibtex file with the result.

# Define the usage function
function usage() {
	echo "Usage: $(basename $0) [--help] [OPTIONS] [--help] arXiv"
	echo
	echo "will query https://arxiv2bibtex.org about the arXiv identified"
	echo "and return the result to stdout in bibtex format."
	echo 
	echo "OPTIONS can be"
	echo "  --raw     does not format the result"
	echo
	echo "Examples:"
	echo "  $(basename $0) 1701.06092"
	echo "  $(basename $0) 1701.06092v1"
	echo "  $(basename $0) https://arxiv.org/abs/1701.06092v1"
	echo "  $(basename $0) math/0602029"
	echo "  $(basename $0) https://arxiv.org/abs/math/0602029"
	echo "  echo 1701.06092 | $(basename $0)"
	echo 
}

# Set debugging flag
_DEBUG=false

# Declare intelligent debug function
# from http://www.cyberciti.biz/tips/debugging-shell-script.html
function DEBUG()
{
 [ "$_DEBUG" = "true" ] && $@
}

# Set the default options
RAW=false

# Parse the command line arguments
# https://medium.com/@Drew_Stokes/bash-argument-parsing-54f3b81a6a8f
# https://www.assertnotmagic.com/2019/03/08/bash-advanced-arguments/
while (( "$#" )); do
	case "$1" in
		--raw)
			RAW=true
			shift
			;;
		
		--help|-*)
			usage
			exit 1
			;;
		
		*)
			# An argument starting with anything but '-' represents a ARXIV.
			# Only one such argument can be given.
			if [ -z "${ARXIV+x}" ]; then
				ARXIV=$1
				DEBUG echo "$ARXIV"
				shift
			else
				usage
				exit 1
			fi
			;;

	esac
done

# Make sure we have an arXiv identifier, either from the command line, or from stdin
if [ -t 0 ]; then
	if [ -z "$ARXIV" ]; then
		usage
		exit 1
	fi
else
	ARXIV=$(cat -)
fi

# Extract the arXiv identifier (e.g., in case given in the form https://arxiv.org/abs/1701.06092v1)
ARXIVPARSED=$(echo "$ARXIV" | egrep '([0-9]{4}\.[0-9]{4,}([v][0-9]*)?|[a-z]{1,}/[0-9]{7})$' -o)
DEBUG echo "$ARXIV"
DEBUG echo "$ARXIVPARSED"
if [ -z "$ARXIVPARSED" ]; then
	echo "$ARXIV does not appear to be a valid arXiv identifier."
	exit 1
fi

# Make sure wget is available
if [ ! -x "$(command -v wget)" ]; then
	echo "$0 depends on wget, which does not seem to be available on your system."
	echo "Please install wget."
	exit 1
fi

# Retrieve the bibtex file using the string() function to ensure utf8 encoding as suggested in
# https://stackoverflow.com/questions/28328199
URL="https://arxiv2bibtex.org/?q=$ARXIVPARSED&format=biblatex"
BIBTEX=$(wget -O - "$URL" -q | xmllint --xpath 'string(//div[@id="biblatex"]/textarea/text())' -)

# Dump the result
if [ "$RAW" = "true" ]; then
	echo "$BIBTEX"
else
	echo "$BIBTEX" 
fi

