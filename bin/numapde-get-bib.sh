#!/bin/bash
# This script queries various data bases for a publication 
# and retrieves a bibtex file with the result(s).
# Optionally, the result(s) are merged into an existing .bib file 
# (avoiding duplicates) and new bibtex keys are generated.

# The query logic is as follows.
# If an arXiv identifier is given, arXiv is queried.
# If a DOI is given, https://doi.org is queried.
# Otherwise, MathSciNet is queried.

# Define the usage function
function usage() {
	echo "Usage: $(basename $0) [--help] [QUERY] [OPTIONS]"
	echo
	echo "will query a number of publication databases"
	echo "and return the result(s) to stdout in bibtex format."
	echo "Optionally, the result(s) are merged into an existing .bib file"
	echo "(avoiding duplicates) and new bibtex keys are generated."
	echo 
	echo "QUERY can be"
	echo "  an arXiv identifier"
	echo "  a DOI"
	echo "  -a, --author" 
	echo "  -j, --journal"
  echo "  -m, --MR"
	echo "  -s, --series"
	echo "  -t, --title" 
	echo "  -y, --year"
	echo
	echo "OPTIONS can be"
	echo "  --raw             does not format the result"
	echo "  --edit            passes the results to \$EDITOR (in your case: $EDITOR)"
	echo "  --merge file.bib  merges the results (dropping duplicates) into file.bib"
	echo "  --update file.bib merges the results (updating duplicates) into file.bib"
	echo
	echo "Examples:"
	echo "  $(basename $0) --author herzog"
	echo "  $(basename $0) -a herzog -t \"superlinear+convergence\""
	echo "  $(basename $0) 1709.01343"
	echo "  $(basename $0) 10.1007/s10851-018-0840-y"
	echo 
}

# Set debugging flag
_DEBUG=false

# Declare intelligent debug function
# from http://www.cyberciti.biz/tips/debugging-shell-script.html
function DEBUG()
{
 [ "$_DEBUG" = "true" ] && $@
}

# Issue help message if necessary
if [ $# = 0 ]; then
	usage
	exit 1
fi

# Set the default options
RAW=
EDIT=0
MERGEFILE=

# Parse the command line arguments
# https://medium.com/@Drew_Stokes/bash-argument-parsing-54f3b81a6a8f
# https://www.assertnotmagic.com/2019/03/08/bash-advanced-arguments/
declare -a MSCARGS
declare -a ARXIVS
declare -a DOIS
while (( "$#" )); do
	case "$1" in
		--help)
			usage
			exit 1
			;;

		--raw)
			RAW="--raw"
			shift
			;;

		--edit)
			EDIT=1
			shift
			;;

		--merge)
			if [ -z "$MERGEFILE" ]; then
				MERGEFILE="$2"
				MERGESWITCH=
			else
				echo "Use only one of --merge or --update."
				echo "Exiting."
				exit 1
			fi
			shift
			shift
			;;
		
		--update)
			if [ -z "$MERGEFILE" ]; then
				MERGEFILE="$2"
				MERGESWITCH="--update"
			else
				echo "Use only one of --merge or --update."
				echo "Exiting."
				exit 1
			fi
			shift
			shift
			;;
		
		-*)
			# Record the argument for later use in querying MathSciNet
			MSCARGS[${#MSCARGS[@]}]="$1"
			MSCARGS[${#MSCARGS[@]}]="$2"
			shift
			shift
			;;

		*)
			# An argument starting with anything but '-' should represent 
			# either an arXiv identifier or a DOI
			# These will be recorded for now
			if $(echo "$1" | egrep -q '10[.][0-9]{4,9}/[-._;()/:a-zA-Z0-9]+'); then
				DOIS[${#DOIS[@]}]="$1"
				DEBUG echo "DOI found: $1"
			elif $(echo "$1" | egrep -q '([0-9]{4}\.[0-9]{4,}([v][0-9]*)?|[a-z]{1,}/[0-9]{7})'); then
				ARXIVS[${#ARXIVS[@]}]="$1"
				DEBUG echo "arXiv identifier found: $1"
			else
				echo "Argument $1 not recognized"
				exit 1
			fi
			shift

	esac
done

DEBUG echo
DEBUG echo "raw flag: $RAW"
DEBUG echo "file into which results are to be merged: $MERGEFILE"
DEBUG echo
DEBUG echo "arXiv identifiers found: ${ARXIVS[@]}"
DEBUG echo "DOIs found: ${DOIS[@]}"
DEBUG echo "MSCARGS found: ${MSCARGS[@]}"
DEBUG echo

# Create a temporary file to store the combined output of all queries
OUTFILE=$(mktemp).bib

# Process all arXiv identifiers
for ARXIV in "${ARXIVS[@]}"; do
	DEBUG echo numapde-query-arxiv.sh "$RAW $ARXIV" 
	numapde-query-arxiv.sh "$RAW $ARXIV" >> "$OUTFILE"
done

# Process all DOIs
for DOI in "${DOIS[@]}"; do
	DEBUG echo numapde-query-doi.sh "$RAW $DOI" 
	numapde-query-doi.sh "$RAW $DOI" >> "$OUTFILE"
done

# Process all MathSciNet queries
if [ ${#MSCARGS[@]} -gt 0 ]; then
	DEBUG echo numapde-query-mathscinet.sh "$RAW ${MSCARGS[@]}"
	numapde-query-mathscinet.sh "${MSCARGS[@]}" >> "$OUTFILE"
fi

# Canonicalize the combined output 
numapde-canonicalize-bibliography.sh "$OUTFILE" | sponge "$OUTFILE"

# Since biber does not do a graceful exit in case of errors 
# (e.g., due to an empty input fule) and dumps errors to stdout, 
# we have to check the output now
if $(grep -q '^ERROR' "$OUTFILE"); then
	exit 1
fi

# Generate preliminary cite keys
numapde-generate-citekeys.py "$OUTFILE" | sponge "$OUTFILE"

# If desired, invoke an editor on the results
if [ "$EDIT" -eq "1" ]; then
	if [ -z "$EDITOR" ]; then
		>&2 echo "Your EDITOR environment variable is empty. Consider setting it."
		EDITOR="$(command -v nano)"
		>&2 echo "For now, we will use $EDITOR."
	fi
	$EDITOR "$OUTFILE"
fi

# Dump the result and exit (unless they are to be merged)
if [ -z "$MERGEFILE" ]; then
	cat "$OUTFILE"
	exit 0
fi

# Merge the results into the file specified
>&2 echo "Merging results into $MERGEFILE"

# Make sure the file to be merged into exists
touch "$MERGEFILE"

# Prepare the merged (or updated) file
numapde-merge-bibliography.py $MERGESWITCH "$OUTFILE" "$MERGEFILE"

# Finalize the cite keys
numapde-finalize-keys.sh "$MERGEFILE" | sponge "$MERGEFILE"

