#!/bin/bash
# This script validates a given BibLaTeX file and issues warnings.
# Checked issues include
# * duplicate cite keys
# * cite keys not following the numapde format
# * extra {} pairs 
# * abbreviated journal names
# * capitalization
# In addition, this script also runs 
#   biber --tool --validate-datamodel 
# on the file.

# Issue help message if necessary
if [ $# = 0 ] && [ -t 0 ] || [ "x$1" = "x--help" ]; then
	echo "Usage: $(basename $0) file.bib"
	echo "       cat file.bib | $(basename $0)"
	echo
	echo "will validate the BibLaTeX file.bib and issue warnings."
	echo "The file.bib will not be modified."
	echo 
	echo "Examples:"
	echo "  $0 numapde.bib"
	echo
	exit 1
fi

# Make sure the input file exists and is readable
# (unless running with piped stdin)
if [ -t 0 ] && [[ ! -r "$1" ]]; then
	echo "Input file $1 not found."
	exit 1
fi

# For unified treatment of piped and non-piped operations, we dump stdin to a temporary file 
if [ -t 0 ]; then
	INFILE="$1"
else
	INFILE=$(mktemp)
	sed -n "w $INFILE"
fi

# Check for duplicate cite keys (exempting temporary keys)
echo
echo "Checking for duplicate cite keys..."
gawk '{ if (match($0, /^@.*{(.*),/, array)) print array[1] }' "$INFILE" | sort | uniq -d |\
	grep -v '.*XX[A-Z]$'

# Check for cite keys with incorrect characters
echo
echo "Checking for cite keys with incorrect characters..."
gawk '{ if (match($0, /^@.*{(.*),/, array)) print array[1] }' "$INFILE" | \
	grep -v '^[a-zA-Z:0-9]\+$'

# Check for cite keys with no lower case characters
echo
echo "Checking for cite keys with no lower case characters..."
gawk '{ if (match($0, /^@.*{(.*),/, array)) print array[1] }' "$INFILE" | \
	grep '^[A-Z:0-9]\+$'

# Check for title fields with no lower case characters
echo
echo "Checking for title fields with no lower case characters..."
EXCEPTIONSFILE=$(dirname $0)/numapde-title-exceptions.txt
gawk '{ if (match($0, /TITLE.*{(.*)},/, array)) print array[1] }' "$INFILE" | \
	grep -v '[a-z]' | \
	grep -v -f "$EXCEPTIONSFILE"

# Check for incorrect cite keys
# Notice that we define here a restrictive set of explicitly allowable characters
# and cite keys following the format or preliminary format
# AuthorAuthor:YYYY:1 
# AuthorAuthor:YYYY:XX[A-Z]
echo
echo "Checking for incorrect cite keys..."
gawk '{ if (match($0, /^@.*{(.*),/, array)) print array[1] }' "$INFILE" | \
	grep -v '^[a-zA-Z]\+:[0-9]\{4\}:\([0-9]\|XX[A-Z]\)$'

# Check for preliminary cite keys
# AuthorAuthor:YYYY:XX[A-Z]
echo
echo "Checking for preliminary cite keys..."
gawk '{ if (match($0, /^@.*{(.*),/, array)) print array[1] }' "$INFILE" | \
	grep '^[a-zA-Z]\+:[0-9]\{4\}:XX[A-Z]$'

# Check for extra {} pairs
# https://superuser.com/questions/135171/listing-lines-from-just-one-file-in-diff
echo
echo "Checking for extra curly braces..."
diff --changed-group-format='%<' --unchanged-group-format='' "$INFILE" <(awk -f $(dirname $0)/numapde-remove-extra-braces.awk "$INFILE")

# Check for abbreviated journal names
echo
echo "Checking for abbreviated journal names..."
awk '/JOURNALTITLE = {.*[{ ]([A-Za-z]{,5}[.][ }]){2,}/' "$INFILE" | \
	grep -v Steklova

# Check for abbreviated series titles
echo
echo "Checking for series titles..."
awk '/SERIES = {.*[{ ]([A-Za-z]{,5}[.][ }]){2,}/' "$INFILE" 

# Check for years instead of date
echo
echo "Checking for year instead of date..."
grep 'YEAR = ' "$INFILE"

# Check for invalid years
echo
echo "Checking for invalid years..."
grep 'DATE =' "$INFILE" | \
	grep -v '{[12][0-9]\{3\}\(-[0-1][0-9]\)\?\(-[0-3][0-9]\)\?}'

# Check for incorrectly escaped ampersands
echo
echo "Checking for incorrectly escaped characters..."
grep '\\\\&' "$INFILE"

# Check for suspicious underscores in DOIs
echo
echo "Checking for suspicious underscores in DOIs..."
grep 'DOI.*\\_' "$INFILE"

# Check for suspicious et al. in editor field
echo
echo "Checking for suspicious et al. in editor field..."
grep 'EDITOR.*et al\.' "$INFILE"

# Check for incorrect title case (many capital letters) in various entry types and fields
echo
echo "Checking for incorrect titlecases..."
EXCEPTIONSFILE=$(dirname $0)/numapde-uppercase-words.txt
awk -f $(dirname $0)/numapde-find-titlecase.awk -v "entry=@ARTICLE"        -v "field= TITLE"     "$EXCEPTIONSFILE" "$INFILE"
awk -f $(dirname $0)/numapde-find-titlecase.awk -v "entry=@INBOOK"         -v "field= TITLE"     "$EXCEPTIONSFILE" "$INFILE"
awk -f $(dirname $0)/numapde-find-titlecase.awk -v "entry=@INCOLLECTION"   -v "field= TITLE"     "$EXCEPTIONSFILE" "$INFILE"
awk -f $(dirname $0)/numapde-find-titlecase.awk -v "entry=@INPROCEEDINGS"  -v "field= TITLE"     "$EXCEPTIONSFILE" "$INFILE"
awk -f $(dirname $0)/numapde-find-titlecase.awk -v "entry=@MANUAL"         -v "field= TITLE"     "$EXCEPTIONSFILE" "$INFILE"
awk -f $(dirname $0)/numapde-find-titlecase.awk -v "entry=@MISC"           -v "field= TITLE"     "$EXCEPTIONSFILE" "$INFILE"
awk -f $(dirname $0)/numapde-find-titlecase.awk -v "entry=@ONLINE"         -v "field= TITLE"     "$EXCEPTIONSFILE" "$INFILE"
awk -f $(dirname $0)/numapde-find-titlecase.awk -v "entry=@REPORT"         -v "field= TITLE"     "$EXCEPTIONSFILE" "$INFILE"

# Check for incorrect sentence case (many non-capital letters) in various entry types and fields
echo
echo "Checking for incorrect sentencecase..."
EXCEPTIONSFILE=$(dirname $0)/numapde-lowercase-words.txt
awk -f $(dirname $0)/numapde-find-sentencecase.awk -v "entry=@BOOK"        -v "field= TITLE"     "$EXCEPTIONSFILE" "$INFILE"
awk -f $(dirname $0)/numapde-find-sentencecase.awk -v "entry=@COLLECTION"  -v "field= TITLE"     "$EXCEPTIONSFILE" "$INFILE"
awk -f $(dirname $0)/numapde-find-sentencecase.awk -v "entry=@PROCEEDINGS" -v "field= TITLE"     "$EXCEPTIONSFILE" "$INFILE"
awk -f $(dirname $0)/numapde-find-sentencecase.awk -v "entry=@THESIS"      -v "field= TITLE"     "$EXCEPTIONSFILE" "$INFILE"
awk -f $(dirname $0)/numapde-find-sentencecase.awk -v "entry=@"            -v "field= BOOKTITLE" "$EXCEPTIONSFILE" "$INFILE"

# Check for authors whose multiple family names may have been incorrectly assigned to given names, such as Núñez, José Vidal
echo
echo "Checking for suspicious author family names..."
EXCEPTIONSFILE=$(dirname $0)/numapde-confirmed-author-names.txt
awk -f $(dirname $0)/numapde-find-suspicious-author-names.awk -v "field= AUTHOR" "$EXCEPTIONSFILE" "$INFILE"
awk -f $(dirname $0)/numapde-find-suspicious-author-names.awk -v "field= EDITOR" "$EXCEPTIONSFILE" "$INFILE"

# Check for missing digital identifiers such as DOIs and arXiv identifiers
echo
echo "Checking for missing digital identifiers..."
EXCEPTIONSFILE=$(dirname $0)/numapde-identifier-exceptions.txt
awk -f $(dirname $0)/numapde-find-missing-identifiers.awk -v "entry=@ARTICLE"       -v "field=DOI|URL"        "$EXCEPTIONSFILE" "$INFILE"
awk -f $(dirname $0)/numapde-find-missing-identifiers.awk -v "entry=@BOOK"          -v "field=DOI|URL"        "$EXCEPTIONSFILE" "$INFILE"
awk -f $(dirname $0)/numapde-find-missing-identifiers.awk -v "entry=@COLLECTION"    -v "field=DOI"            "$EXCEPTIONSFILE" "$INFILE"
awk -f $(dirname $0)/numapde-find-missing-identifiers.awk -v "entry=@INBOOK"        -v "field=DOI"            "$EXCEPTIONSFILE" "$INFILE"
awk -f $(dirname $0)/numapde-find-missing-identifiers.awk -v "entry=@INCOLLECTION"  -v "field=DOI|URL"        "$EXCEPTIONSFILE" "$INFILE"
awk -f $(dirname $0)/numapde-find-missing-identifiers.awk -v "entry=@INPROCEEDINGS" -v "field=DOI|EPRINT|URL" "$EXCEPTIONSFILE" "$INFILE"
awk -f $(dirname $0)/numapde-find-missing-identifiers.awk -v "entry=@ONLINE"        -v "field=EPRINT|URL"     "$EXCEPTIONSFILE" "$INFILE"
awk -f $(dirname $0)/numapde-find-missing-identifiers.awk -v "entry=@PROCEEDINGS"   -v "field=DOI"            "$EXCEPTIONSFILE" "$INFILE"

# Report on the number of entries in .bib file
echo
printf "The file $INFILE contains %d entries.\n" $(grep '^@' "$INFILE" | wc -l)

# Validate the data model
# This takes about 30 seconds to run
# echo
# echo "Validating data model..."
# echo TODO specify outfile
# biber --tool --quiet --validate-datamodel "$INFILE"

