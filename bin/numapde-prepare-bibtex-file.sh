#!/bin/bash
# This script prepares a legacy BibTeX file from one or several BibLaTeX 
# sources. In particular, it
# * converts UTF8 characters into their LaTeX transcriptions
# * converts entry types such as @THESIS into BibTeX compatible types
# This script can operate 
# * in a document oriented mode, where the bibliography is drawn from a 
#   document's .bcf file (using the data source(s) declared there)
# * in a database oriented mode, where an entire .bib file is 
#   converted.

# Define the usage function
function usage() {
	echo "Usage: $(basename $0) [--help] [OPTIONS] source.{bcf,bib}"
	echo
	echo "In document oriented mode, this script converts the references contained"
	echo "in source.bcf (using the data sources described therein) into a BibTeX"
	echo "compatible output on stdout."
	echo "In database oriented mode, this script converts source.bib into the BibTeX"
	echo "compatible output on stdout."
	echo 
	echo "OPTIONS can be"
	echo "  --arxiv           transcribe EPRINTTYPE = {arXiv} field into"
	echo "                    TYPE or NOTE fields"
	echo "  --doinote         transcribe DOI = {doi} field into a NOTE field"
	echo "                    with a link to https://doi.org/doi"
	echo "  --doiurl          transcribe DOI = {doi} field into a URL field"
	echo "                    https://doi.org/doi"
	echo "  --giveninits      abbreviate authors' and editors' given names"
	echo "  --hal             transcribe EPRINTTYPE = {HAL} field into"
	echo "                    TYPE or NOTE fields"
	echo "  --keeputf8        do not transcode to BibTeX charset"
	echo "  --noprotect       do not protect upper case characters"
	echo "  --urlnote         transcribe URL = {url} field into a NOTE field"
	echo "                    with a link to url"
	echo "  --urn             transcribe EPRINTTYPE = {urn} field into"
	echo "                    NOTE field"
	echo "  --online          transcribe @ONLINE entries into @TECHREPORT"
	echo "  --masterSthesis   transcribe @MASTERTHESIS into @MASTERSTHESIS"
	echo
	echo "Examples:"
	echo "  $(basename $0) --arxiv manuscript-numapde-preprint.bcf"
	echo "  $(basename $0) --online --arxiv numapde-local.bib"
	echo 
}

# Define a version comparison function
# https://unix.stackexchange.com/questions/285924/how-to-compare-a-programs-version-in-a-shell-script
function version_greater_equal() { 
	SMALLEROFTWO=$(printf '%s\n%s\n' "$1" "$2" | sort -V | head -n 1)
	[[ "$SMALLEROFTWO" =~ $2 ]]
}

# Set debugging flag
_DEBUG=false

# Set the default options
ABBREVIATEGIVENNAMES=false
PROTECTUPPERCASE=true
TOBIBTEXCHARSET=true
TRANSCRIBEONLINE=false
TRANSCRIBEARXIV=false
TRANSCRIBEDOINOTE=false
TRANSCRIBEDOIURL=false
TRANSCRIBEURLNOTE=false
TRANSCRIBEHAL=false
TRANSCRIBEURN=false
TRANSCRIBEMASTERTHESIS=false

# Declare intelligent debug function
# from http://www.cyberciti.biz/tips/debugging-shell-script.html
function DEBUG()
{
 [ "$_DEBUG" = "true" ] && $@
}

# Store how the script was invoked
# https://stackoverflow.com/questions/36625593/how-to-get-the-complete-calling-command-of-a-bash-script-from-inside-the-script
MYINVOCATION="$(printf %q "$(basename $BASH_SOURCE)")$( (($#)) && printf ' %q' "$@")"

# Parse the command line arguments
# https://medium.com/@Drew_Stokes/bash-argument-parsing-54f3b81a6a8f
# https://www.assertnotmagic.com/2019/03/08/bash-advanced-arguments/
while (( "$#" )); do
	case "$1" in
		--keeputf8)
			TOBIBTEXCHARSET=false
			shift
			;;

		--noprotect)
			PROTECTUPPERCASE=false
			shift
			;;

		--online)
			TRANSCRIBEONLINE=true
			shift
			;;

		--masterSthesis)
			TRANSCRIBEMASTERTHESIS=true
			shift
			;;

		--arxiv)
			TRANSCRIBEARXIV=true
			shift
			;;

		--hal)
			TRANSCRIBEHAL=true
			shift
			;;

		--urn)
			TRANSCRIBEURN=true
			shift
			;;

		--doinote)
			TRANSCRIBEDOINOTE=true
			shift
			;;

		--doiurl)
			TRANSCRIBEDOIURL=true
			shift
			;;

		--urlnote)
			TRANSCRIBEURLNOTE=true
			shift
			;;

		--giveninits)
			ABBREVIATEGIVENNAMES=true
			shift
			;;

		--help|-*)
			usage
			exit 1
			;;

		*)
			# An argument starting with anything but '-' represents the source file.
			# Only one such argument can be given.
			if [ -z "${INFILE+x}" ]; then
				INFILE="$1"
				DEBUG >&2 echo "Reading from $INFILE"
				shift
			else
				usage
				exit 1
			fi
			;;
	esac
done

# If no source is specified, print help and exit
if [ -z "$INFILE" ]; then
	usage
	exit 1
fi

# Make sure the input file exists and is readable
if [[ ! -r "$INFILE" ]]; then
	>&2 echo "Input file $INFILE not found."
	exit 1
fi

# Determine whether we are in document or database oriented mode
DEBUG >&2 echo "Input file type: ${INFILE#*.}"
if [ ${INFILE#*.} = "bcf" ]; then
	MODE=document
elif [ ${INFILE#*.} = "bib" ]; then
	MODE=database
else
	>&2 echo "source must be either a .bcf or a .bib file."
	exit 1
fi

# Create a temporary file to store the output .bib file
OUTFILE=$(mktemp /tmp/tmp.XXXXXXXXXX.bib) 
DEBUG >&2 echo "Temporary file $OUTFILE"


# Due to a bug in biber, we need version 2.10 or later for document mode
if [ "$MODE" = "document" ]; then
	BIBERVERSION=$(biber --version | cut -f2 -d: | tr -d '[:space:]')
	version_greater_equal "$BIBERVERSION" "2.10"
	STATUS=$?
	if ! $(exit $STATUS); then
		>&2 echo "In document mode, biber version must be at least 2.10 but yours is only $BIBERVERSION."
		exit 1
	fi
fi

# Are we going to convert to LaTeX charset?
BBLSAFECHARS=
if [ "$TOBIBTEXCHARSET" = "true" ]; then
	BBLSAFECHARS="--bblsafechars"
fi

# Use biber to prepare an initial output .bib file, possibly with UTF8 characters 
# replaced by their LaTeX equivalents
if [ "$MODE" = "document" ]; then
	biber --quiet        $BBLSAFECHARS --output-format=bibtex --output-file="$OUTFILE" "$INFILE"
else
	biber --quiet --tool $BBLSAFECHARS --output-format=bibtex --output-file="$OUTFILE" "$INFILE"
fi


# Prepend a time stamp to the output .bib file
{ echo -e "@COMMENT{\nGenerated by $(basename $0) on $(date +%Y%m%d-%H:%M:%S)\n$MYINVOCATION\n}\n"; cat $OUTFILE; } | sponge "$OUTFILE" 

# Are we going to convert to LaTeX charset?
if [ "$PROTECTUPPERCASE" = "true" ]; then

	# In all TITLE, SUBTITLE, BOOKTITLE fields, protect all consecutive chains of uppercase letters by braces 
	for FIELD in TITLE SUBTITLE BOOKTITLE; do 
		SEDARGS=()
		for LINENUMBER in $(grep -n ' '"$FIELD"' = {' "$OUTFILE" | cut -f1 -d:); do
			SEDARGS+=" -e ${LINENUMBER}s/\([A-Z]\+\)/{\1}/2g"
		done
		if test ${#SEDARGS[@]} -gt 0; then
			DEBUG >&2 echo "sed -i ${SEDARGS[@]} $OUTFILE"
			sed -i ${SEDARGS[@]} "$OUTFILE"
		fi
	done

fi


# Convert all DATE to YEAR fields, preserving the first four digits of the actual date
sed -i 's/DATE\( = {[0-9]\{,4\}\).*$/YEAR\1},/' "$OUTFILE"

# Replace all LOCATION by ADDRESS fields
sed -i 's/LOCATION =/ADDRESS =/' "$OUTFILE"

# Replace all JOURNALTITLE by JOURNAL fields
sed -i 's/JOURNALTITLE =/JOURNAL =/' "$OUTFILE"

# Replace all @REPORT by @TECHREPORT entries
sed -i 's/@REPORT/@TECHREPORT/' "$OUTFILE"
sed -i 's/{techreport}/{Technical report}/' "$OUTFILE"

# Replace all @COLLECTION by @BOOK entries
sed -i 's/@COLLECTION/@BOOK/' "$OUTFILE"

# Replace all ORGANIZATION by PUBLISHER fields
sed -i 's/ORGANIZATION =/PUBLISHER =/' "$OUTFILE"


# Convert @THESIS with TYPE = "Bachelor thesis" to @MASTERTHESIS, and keep the TYPE
# Replace INSTITUTION by SCHOOL
awk 'BEGIN { RS = "\n\n+" } 
{ 
	if ($0 ~ /^@THESIS.*TYPE = {Bachelor thesis}/) { 
		sub(/INSTITUTION =/, "SCHOOL =", $0)
		sub(/@THESIS/, "@MASTERTHESIS", $0)
		sub(/TYPE = {Bachelor thesis}/, "TYPE = {{B}achelor thesis}", $0);
	}
	print $0 "\n"
}' "$OUTFILE" | sponge "$OUTFILE"


# Convert @THESIS with TYPE = "mathesis" to @MASTERTHESIS, and modify the TYPE
awk 'BEGIN { RS = "\n\n+" } 
{ 
	if ($0 ~ /^@THESIS.*TYPE = {mathesis}/) { 
		sub(/INSTITUTION =/, "SCHOOL =", $0)
		sub(/@THESIS/, "@MASTERTHESIS", $0)
		sub(/TYPE = {mathesis}/, "TYPE = {{M}aster thesis}", $0);
	}
	print $0 "\n"
}' "$OUTFILE" | sponge "$OUTFILE"


# Convert @THESIS with TYPE = "phdthesis" to @MASTERTHESIS, and modify the TYPE
awk 'BEGIN { RS = "\n\n+" } 
{ 
	if ($0 ~ /^@THESIS.*TYPE = {phdthesis}/) { 
		sub(/INSTITUTION =/, "SCHOOL =", $0)
		sub(/@THESIS/, "@PHDTHESIS", $0)
		sub(/TYPE = {phdthesis}/, "TYPE = {{P}h.{D}. thesis}", $0);
	}
	print $0 "\n"
}' "$OUTFILE" | sponge "$OUTFILE"


# Convert @THESIS with TYPE = "Habilitation thesis" to @PHDTHESIS, and keep the TYPE
awk 'BEGIN { RS = "\n\n+" } 
{ 
	if ($0 ~ /^@THESIS.*TYPE = {Habilitation thesis}/) { 
		sub(/INSTITUTION =/, "SCHOOL =", $0)
		sub(/@THESIS/, "@PHDTHESIS", $0)
		sub(/TYPE = {Habilitation thesis}/, "TYPE = {{H}abilitation thesis}", $0);
	}
	print $0 "\n"
}' "$OUTFILE" | sponge "$OUTFILE"


# Append content of SUBTITLE field to the TITLE field
# Leave SUBTITLE in since it does no harm
awk 'BEGIN { RS = "\n\n+" } 
{ 
	if (match($0, /SUBTITLE = {([^\n]*)},[ \t]*\n/, subtitle)) {
		$0 = gensub(/([^B]TITLE = ){([^\n]*)},[ \t]*\n/, "\\1{\\2. " subtitle[1]"},", "g", $0)
	}
	print $0 "\n"
}' "$OUTFILE" | sponge "$OUTFILE"


# Are we going to transcribe @ONLINE into @TECHREPORT entries?
if [ "$TRANSCRIBEONLINE" = "true" ]; then
	# Convert all @ONLINE into @TECHREPORT
	sed -i 's/@ONLINE/@TECHREPORT/' "$OUTFILE"
fi


# Are we going to transcribe @MASTERTHESIS into @MASTERSTHESIS entries?
if [ "$TRANSCRIBEMASTERTHESIS" = "true" ]; then
	# Convert all @MASTERTHESIS into @MASTERSTHESIS
	sed -i 's/@MASTERTHESIS/@MASTERSTHESIS/' "$OUTFILE"
fi


# Are we going to transcribe EPRINTTTYPE = {arXiv} fields?
if [ "$TRANSCRIBEARXIV" = "true" ]; then
	# In @TECHREPORT, generate a TYPE field from EPRINT field 
	# In all other entry types (for example, @ARTICLE), generate a NOTE field from EPRINT field 
	# Leave EPRINTTYPE in since it does no harm
	awk 'BEGIN { RS = "\n\n+" } 
	{ 
		if ($0 ~ /^@TECHREPORT{.*EPRINTTYPE = {ar[xX]iv}/) { $0 = gensub(/EPRINT = {([^}]+)},/, "TYPE = {{arXiv}: \\\\href{https://arxiv.org/abs/\\1}{\\1}},", 1, $0) } 
		else if ($0 ~ /EPRINTTYPE = {ar[xX]iv}/) { $0 = gensub(/EPRINT = {([^}]+)},/, "NOTE = {{arXiv}: \\\\href{https://arxiv.org/abs/\\1}{\\1}},", 1, $0) } 
		print $0 "\n" 
	}' "$OUTFILE" | sponge "$OUTFILE"
fi


# Are we going to transcribe EPRINTTTYPE = {HAL} fields?
if [ "$TRANSCRIBEHAL" = "true" ]; then
	# In @TECHREPORT, generate a TYPE field from EPRINT field 
	# In all other entry types (for example, @ARTICLE), generate a NOTE field from EPRINT field 
	# Leave EPRINTTYPE in since it does no harm
	awk 'BEGIN { RS = "\n\n+" } 
	{ 
		if ($0 ~ /^@TECHREPORT{.*EPRINTTYPE = {HAL}/) { $0 = gensub(/EPRINT = {([^}]+)},/, "TYPE = {{HAL}: \\\\href{https://hal.archives-ouvertes.fr/\\1}{\\1}},", 1, $0) } 
		else if ($0 ~ /EPRINTTYPE = {HAL}/) { $0 = gensub(/EPRINT = {([^}]+)},/, "NOTE = {{HAL}: \\\\href{https://hal.archives-ouvertes.fr/\\1}{\\1}},", 1, $0) } 
		print $0 "\n" 
	}' "$OUTFILE" | sponge "$OUTFILE"
fi


# Are we going to transcribe EPRINTTTYPE = {urn} fields?
if [ "$TRANSCRIBEURN" = "true" ]; then
	# In all entry types, generate a NOTE field from EPRINT field 
	# Leave EPRINTTYPE in since it does no harm
	awk 'BEGIN { RS = "\n\n+" } 
	{ 
		if ($0 ~ /EPRINTTYPE = {urn}/) { $0 = gensub(/EPRINT = {([^}]+)},/, "NOTE = {{URN}: \\\\href{https://www.nbn-resolving.de/\\1}{\\\\detokenize{\\1}}},", 1, $0) } 
		print $0 "\n" 
	}' "$OUTFILE" | sponge "$OUTFILE"
fi


# Are we going to transcribe DOI into URL fields?
if [ "$TRANSCRIBEDOIURL" = "true" ]; then
	# In all entry types, generate a URL field from DOI field 
	# Leave DOI in since it does no harm
	awk 'BEGIN { RS = "\n\n+" } 
	{ 
		if ($0 ~ /DOI = {/) { $0 = gensub(/DOI = {([^}]+)},/, "URL = {https://doi.org/\\1},", 1, $0) } 
		print $0 "\n" 
	}' "$OUTFILE" | sponge "$OUTFILE"
fi


# Are we going to transcribe DOI into NOTE fields?
if [ "$TRANSCRIBEDOINOTE" = "true" ]; then
	# In all entry types, generate a NOTE field from DOI field 
	# Leave DOI in since it does no harm
	awk 'BEGIN { RS = "\n\n+" } 
	{ 
		if ($0 ~ /DOI = {/) { $0 = gensub(/DOI = {([^}]+)},/, "NOTE = {{DOI} \\\\href{https://doi.org/\\1}{\\\\detokenize{\\1}}},", 1, $0) } 
		print $0 "\n" 
	}' "$OUTFILE" | sponge "$OUTFILE"
fi


# Are we going to transcribe DOI into NOTE fields?
if [ "$TRANSCRIBEURLNOTE" = "true" ]; then
	# In all entry types, generate a NOTE field from URL field 
	# Leave URL in since it does no harm
	awk 'BEGIN { RS = "\n\n+" } 
	{ 
		if ($0 ~ /URL = {/) { $0 = gensub(/URL = {([^}]+)},/, "NOTE = {\\\\url{\\1}},", 1, $0) } 
		print $0 "\n" 
	}' "$OUTFILE" | sponge "$OUTFILE"
fi


# Are we going to abbreviate authors' and editors' given names?
if [ "$ABBREVIATEGIVENNAMES" = "true" ]; then
	# Invoke the script which will carry out the abbreviation
	awk -f $(dirname $0)/numapde-abbreviate-given-names.awk -v "field=AUTHOR"  "$OUTFILE" | sponge "$OUTFILE"
	awk -f $(dirname $0)/numapde-abbreviate-given-names.awk -v "field=EDITOR"  "$OUTFILE" | sponge "$OUTFILE"
fi

# Eventually, dump the output to stdout
cat "$OUTFILE"
