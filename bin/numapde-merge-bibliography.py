#!/usr/bin/env python3
# vim:set et sw=2 ts=2:

# Merge new bibliography entries into an existing .bib file. Two entries are considered duplicates if
# any of the following is true:
# * they share the same DOI
# * they share the same EPRINT identifier

maintainers = ('Andreas Naumann, Roland Herzog')

# Resolve the dependencies
try:
  import pybtex.database as db
except ImportError:
  print('Please install pybtex using\n  pip3 install pybtex\n')
  raise

# Resolve the dependencies
import sys, os
import argparse
import re
from subprocess import run, PIPE

def _decanonicalize(bibDB):
  """ bibtex always decodes the output and therefore escapes several symbols, sometimes even multiple times. As we assume, that the input is already correctly encoded, we can remove several additional escape sequences.
  """
  bibEntryAsString = bibDB.to_string('bibtex')
  bibEntryAsString = bibEntryAsString.replace('\\\\','\\')
  # split into lines and remove all backslashes in the DOI line
  asLines = bibEntryAsString.splitlines()
  repl = None
  for pos, line in enumerate(asLines):
    if 'DOI =' in line:
      repl = line.replace('\\', '')
      break
  if repl:
    asLines[pos] = repl
  bibEntryAsString = '\n'.join(asLines)
  return bibEntryAsString

# Define some examples for the help text
epilog = r"""

Examples:
    {scriptName} new.bib old.bib 
    
Maintainers: {maintainers}""".format(scriptName = sys.argv[0], maintainers = maintainers)

# use the canonicalize script from the same folder as the merge script
myDir = os.path.abspath(os.path.dirname(__file__))
canonicalize = os.path.join(myDir, 'numapde-canonicalize-bibliography.sh')

# Provide the command line arguments to the parser
parser = argparse.ArgumentParser(description = 'This script merges a Bib(La)TeX library (new.bib) into another one (old.bib).\nBy default,  into old.bib, dropping (and reporting) duplicate entries.', epilog = epilog , formatter_class = argparse.RawTextHelpFormatter)
parser.add_argument('new', metavar = 'new', help = 'the new.bib file containing new entries')
parser.add_argument('old', metavar = 'old', help = 'the old.bib file into which new.bib will be merged')
parser.add_argument('--update', dest='update', default = False, action='store_true', help = 'update existing entries and add new entries')
args = parser.parse_args()

# Parse the old and new .bib files
if args.update:
  oldAsString = open(args.old, 'r').read()
  oldData = db.parse_string(oldAsString, 'bibtex')
  oldAsString = oldAsString.splitlines()
else:
  oldAsString = None
  oldData = db.parse_file(args.old)
newData = db.parse_file(args.new)

# store the new DOI and EPRINT values (in general there are fewer new than old entries)
newDOIs = {}
newEPRINTS = {}
newKeys = set()
for newKey in newData.entries:
  entry = newData.entries[newKey]
  newKeys.add(newKey)
  curDOI = entry.fields.get('DOI')
  curEPRINT = entry.fields.get('EPRINT')
  if curDOI:
    newDOIs[curDOI.upper()] = newKey
  if curEPRINT:
    newEPRINTS[curEPRINT] = newKey

duplicates = set()
# Check the old library fields against the new ones
for oldKey in oldData.entries:
  entry = oldData.entries[oldKey]
  curDOI = entry.fields.get('DOI', None)
  if curDOI:
    curDOI = curDOI.upper()
  curEPRINT = entry.fields.get('EPRINT', None)
  removeFromNew = set()
  if(curDOI in newDOIs):
    duplicates.add((newDOIs[curDOI],'DOI', oldKey))
    removeFromNew.add(newDOIs[curDOI])
    del newDOIs[curDOI]
  if(curEPRINT in newEPRINTS):
    duplicates.add((newEPRINTS[curEPRINT], 'EPRINT', oldKey))
    removeFromNew.add(newEPRINTS[curEPRINT])
    del newEPRINTS[curEPRINT]
  for r in removeFromNew:
    # Remove the key if it already exists (we may have double matches, e.g., DOI and arXiv)
    if r in newKeys:
      newKeys.remove(r)

newDB = db.BibliographyData()
for newKey in newKeys:
  print('Adding the new entry {}'.format(newKey))
  newDB.add_entry(newKey, newData.entries[newKey])

if args.update:
  updatedOldKeys = set()
  for newKey in duplicates:
    newBibKey = newKey[0].split(':')[0]
    oldBibKey = newKey[2].split(':')[0]
    if newBibKey != oldBibKey:
      print('the updated entry {} (with {} value as existing key {}) was discarded due to different authors and changed bibkey from {} to {}'.format(*newKey, oldBibKey, newBibKey))
      continue
    updatedEntry = oldData.entries[newKey[2]]
    updatedEntry.fields.update(newData.entries[newKey[0]].fields)
    updatedOldKeys.add(newKey[2])
    print('updated the entry {}'.format(newKey))
  # pybtex writes the bibtex file in a different style. Rewriting the whole file would therefore change too much.
  # Instead we collect the lines with the updated entries and rewrite only the updated entries
  keyReg = re.compile('^@.*{(.*),')
  endReg = re.compile('^}')
  updatedLines = []
  findKey = True # flag the mode (findKey vs findEnd)
  for lineNr, line in enumerate(oldAsString):
    if findKey:
      m = keyReg.match(line)
      if m and m.group(1) in updatedOldKeys:
        findKey = False
        begLine = lineNr
        begKey  = m.group(1)
    else:
      m = endReg.match(line)
      if m:
        updatedLines.append((begKey, begLine, lineNr))
        findKey = True
        begLine = None
        begKey  = None
  # sort by begLine
  updatedLines = sorted(updatedLines, key= lambda value: value[1])
  # due to the update, we have to rewrite the whole file. 
  with open(args.old, 'w') as oldFile:
    # construct the blocks
    blockBeg = 0
    for ignoreBlock in updatedLines:
      # write the non-changed part
      blockEnd = ignoreBlock[1]
      block = '\n'.join(oldAsString[blockBeg:blockEnd])
      oldFile.write(block)
      # generate a small bibliography with the updated entry
      # then canonicalize the updated entry and write the new string
      subLib = db.BibliographyData({ignoreBlock[0] : oldData.entries[ignoreBlock[0]]})
      canonicalizeReturn = run([canonicalize], input=_decanonicalize(subLib), stdout=PIPE, universal_newlines=True)
      oldFile.write(canonicalizeReturn.stdout)
      blockBeg = ignoreBlock[2] + 1
    block = '\n'.join(oldAsString[blockBeg:len(oldAsString)])
    # git does not like a missing newline.. so append it
    oldFile.write(block + '\n')
else:
  for newKey in duplicates:
    print('Discarding the new entry {} (same {} value as existing key {})'.format(*newKey))

# pybtex has a very different style, so appending the new keys changes less in the old file
# we already call canonicalize on the updated entries. so we can also do that with the new ones.
# canonicalize does not like empty libraries
if len(newDB.entries) > 0:
  with open(args.old, 'a') as oldFile:
    canonicalizeReturn = run([canonicalize], input=_decanonicalize(newDB), stdout=PIPE, universal_newlines=True)
    oldFile.write(canonicalizeReturn.stdout)

