#!/bin/bash
# This script finalizes incomplete cite keys of the form
#   AuthorAuthor:YYYY:XX[A-Z]
# in a given BibLaTeX file file.bib and replaces the
# disambiguation counter XX[A-Z] either by 1 or by the number 
# following the highest existing cite key agreeing in the
# Author:YYYY: (termed the prefix) part.

# Issue help message if necessary
if [ $# = 0 ] && [ -t 0 ] || [ "x$1" = "x--help" ]; then
	echo "Usage: $(basename $0) file.bib"
	echo "       cat file.bib | $(basename $0)"
	echo
	echo "will replace incomplete cite keys in the BibLaTeX file.bib."
	echo "The file.bib is not modified. The result is dumped to stdout."
	echo 
	echo "Examples:"
	echo "  $0 numapde.bib"
	echo "  $0 numapde.bib | sponge numapde.bib"
	echo
	exit 1
fi

# Set debugging flag
_DEBUG=false

# Declare intelligent debug function
# from http://www.cyberciti.biz/tips/debugging-shell-script.html
function DEBUG()
{
	[ "$_DEBUG" = "true" ] && $@
}

# Make sure the input file exists and is readable
# (unless running with piped stdin)
if [ -t 0 ] && [[ ! -r "$1" ]]; then
	echo "Input file $1 not found."
	exit 1
fi

# For unified treatment of piped and non-piped operations, we dump stdin to a temporary file 
if [ -t 0 ]; then
	INFILE="$1"
else
	INFILE=$(mktemp)
	sed -n "w $INFILE"
fi

# Find all keys to be replaced, and record the line numbers they are found in,
# the part of the line before the key, as well as the key itself
counter=0
LINENUMBERS=()
LINESBEFOREKEY=()
KEYSTOBEREPLACED=()
SUFFSFORKEYS=()
while read -r line; do
	read -r LINENUMBER LINEBEFOREKEY KEYTOBEREPLACED SUFFFORKEY <<< "$line"
	LINENUMBERS+=($LINENUMBER)
	LINESBEFOREKEY+=($LINEBEFOREKEY)
	KEYSTOBEREPLACED+=($KEYTOBEREPLACED)
	SUFFSFORKEYS+=($SUFFFORKEY)
done < <(gawk '{ if (match($0, /^(@.*{)(.*:)(XX[A-Z]),/, array)) print NR " " array[1] " " array[2] " " array[3] }' "$INFILE")

# Report how many keys are to be replaced
NUMBERREPLACEMENTS="${#LINENUMBERS[@]}"
if [ "$NUMBERREPLACEMENTS" -eq "0" ]; then
	echo "Nothing to be done." >&2
	cat "$INFILE"
	exit 0
elif [ "$NUMBERREPLACEMENTS" -eq "1" ]; then
	echo "Found $NUMBERREPLACEMENTS key to be replaced." >&2
else
	echo "Found $NUMBERREPLACEMENTS keys to be replaced." >&2
fi
for i in ${!LINENUMBERS[@]}; do
	DEBUG >&2 echo "In line ${LINENUMBERS[$i]} we need to replace ${LINESBEFOREKEY[$i]}${KEYSTOBEREPLACED[$i]}"
done

# Loop over the keys to be replaced, find all other instances of the key 
# (with XX[A-Z] replaced by some single digit number)
# and record the one with the highest number in an associative array
declare -A LASTKEYS
DEBUG >&2 echo "Recorded the following last keys:"
for i in ${!LINENUMBERS[@]}; do
	# Determine the prefix part of the key by removing the trailing XX[A-Z]
	KEYSUFFIX=${SUFFSFORKEYS[$i]}
	KEYPREFIX=${KEYSTOBEREPLACED[$i]%%"$KEYSUFFIX"}
	# Find all other instances of the key (with XX[A-Z] replaced by a single digit number)
	# and record the one with the highest number (if any), otherwise record 0
	LASTKEYSAMEPREFIX=$(grep -o "{${KEYPREFIX}[0-9]" "$INFILE" | sort | tail -1)
	DEBUG >&2 echo "Result of grep with tail for the prefix ${KEYPREFIX} gives ${LASTKEYSAMEPREFIX}"
	if [[ -z $LASTKEYSAMEPREFIX ]]; then
		LASTKEYS[$KEYPREFIX]=0
		DEBUG >&2 echo "The last entry matching ${KEYSTOBEREPLACED[$i]} is NONE yielding number ${LASTKEYS[$KEYPREFIX]}"
	else
		LASTKEYS[$KEYPREFIX]=${LASTKEYSAMEPREFIX##"{${KEYPREFIX}"}
		DEBUG >&2 echo "The last entry matching ${KEYSTOBEREPLACED[$i]} is $LASTKEYSAMEPREFIX yielding number ${LASTKEYS[$KEYPREFIX]}"
	fi
done

# Loop over the keys to be replaced, find the key to replace it with and prepare
# a replacement command for sed
# https://stackoverflow.com/questions/25563024
KEYSREPLACEMENTS=()
SEDARGS=()
for i in ${!LINENUMBERS[@]}; do
	# Determine the prefix part of the key by removing the trailing suffix, which as extracted before
	KEYSUFFIX=${SUFFSFORKEYS[$i]}
	KEYPREFIX=${KEYSTOBEREPLACED[$i]%%"$KEYSUFFIX"}
	>&2 echo "KEYPREFIX = ${KEYPREFIX}"
	>&2 echo "KEYSUFFIX = ${KEYSUFFIX}"
	>&2 echo "LASTKEYS = $((LASTKEYS[$KEYPREFIX]))"
	# Create the replacement string, incrementing the last used instance of the current prefix on record
	KEYSREPLACEMENTS[$i]="${KEYPREFIX}$((++LASTKEYS[$KEYPREFIX]))"
	>&2 echo "KEYSREPLACEMENTS = ${KEYSREPLACEMENTS[$i]}"
	CURRENTKEY="${KEYSTOBEREPLACED[$i]}$KEYSUFFIX"
	>&2 echo "Replacing ${CURRENTKEY} with ${KEYSREPLACEMENTS[$i]}"
	SEDARGS+=" -e ${LINENUMBERS[$i]}s/${CURRENTKEY}/${KEYSREPLACEMENTS[$i]}/"
done

# Apply sed to do the actual replacements
DEBUG >&2 echo "sed ${SEDARGS[@]} $INFILE"
sed ${SEDARGS[@]} $INFILE

