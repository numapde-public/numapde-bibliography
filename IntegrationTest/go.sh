#!/bin/bash
# This script compiles the bibliography dump of numapde.bib.
# Due to the size of the bibliography, the dump is split into two files,
# articles and non-articles.

# Create the master files for the articles and non-articles bibliography dumps
numapde-prepare-manuscript.py --infile numapde-bibliography-dump.yaml --outname numapde-bibliography-dump-articles --nosuffix --main numapde-bibliography-dump-articles-main.tex
numapde-prepare-manuscript.py --infile numapde-bibliography-dump.yaml --outname numapde-bibliography-dump-nonarticles --nosuffix --main numapde-bibliography-dump-nonarticles-main.tex

# Find all articles, and generate \cite{} commands for them in 
# numapde-bibliography-dump-articles-includes.tex
awk -v FS=[{,] '/^@ARTICLE/ {print "\\nocite{" $2 "}"}' ../numapde.bib > numapde-bibliography-dump-articles-includes.tex

# Find all non-articles, and generate \cite{} commands for them in 
# numapde-bibliography-dump-nonarticles-includes.tex
awk -v FS=[{,] '/^@[B-Z]/ {print "\\nocite{" $2 "}"}' ../numapde.bib > numapde-bibliography-dump-nonarticles-includes.tex

# Compile the articles and non-articles bibliography dumps
latexmk -pdf -interaction=batchmode numapde-bibliography-dump-articles.tex
latexmk -pdf -interaction=batchmode numapde-bibliography-dump-nonarticles.tex

