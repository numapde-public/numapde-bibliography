#!/usr/bin/bash

# Update package list
sudo apt-get update

# Install required packages 
sudo apt-get --no-install-recommends -y install gitlab-runner texlive-full python3 recode python3-pip gawk biber coreutils grep perl diffutils moreutils wget libxml2-utils python3-setuptools python3-wheel curl

# Start the gitlab-runner
sudo gitlab-runner start

# Continue as user gitlab-runner
sudo -u gitlab-runner -s

# Install pybtex for user gitlab-runner
pip3 install pybtex

# Generate an ssh key pair if it does not exist
if [ ! -f ~/.ssh/id_rsa ]; then
	ssh-keygen -t rsa -b 4096 -N "" -f ~/.ssh/id_rsa
fi

# Copy the public ssh key to the gitlab server
echo
echo "Copying the public key to the gitlab server"
curl --silent --header "Private-Token: ${NUMAPDE_GITLAB_TOKEN}" -d '{"title":"gitlab-runner-numapde", "key":"'"$(cat ~/.ssh/id_rsa.pub)"'" }' -H "Content-Type: application/json" -X POST https://${NUMAPDE_GITLAB_SERVER}/api/v4/user/keys
echo

# Retrieve and store (replace) ssh key from gitlab server
ssh-keygen -R ${NUMAPDE_GITLAB_SERVER}
ssh-keyscan -t rsa ${NUMAPDE_GITLAB_SERVER} >> ~/.ssh/known_hosts

# Configure git global variables
git config --global user.email "roland.herzog@mathematik.tu-chemnitz.de"
git config --global user.name "numapde CI/CD service"

