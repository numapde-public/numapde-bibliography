# Configuration of the CI system

## Description
The continuous integration in gitlab is a three stage server client architecture:
1. [gitlab server](https://gitlab.hrz.tu-chemnitz.de)
2. [gitlab runner](https://docs.gitlab.com/runner/)
3. [executor](https://docs.gitlab.com/runner/executors/README.html)

While the gitlab server machine is in general distinct from the runner and executor, the latter two run on the same machine **cicd.mathematik.tu-chemnitz.de**.

This README.md describes 
1. the installation of a gitlab runner with a shell executor on the same host **cicd.mathematik.tu-chemnitz.de**. These steps have to be repeated whenever the CI runners associated with this repository require additional or updated packages.
2. the configuration of additional runners for this repository.

## Installation and preparation on cicd.mathematik.tu-chemnitz.de
### Installation requirements
The testing infrastructure requires:

- an Ubuntu based host system
- at least maintainer privileges of the repository

### Installation steps
Run the script 
```
ssh ${NUMAPDE_MRZ_LOGIN}@${NUMAPDE_GITLAB_CISERVER} NUMAPDE_GITLAB_SERVER="${NUMAPDE_GITLAB_SERVER}" NUMAPDE_GITLAB_TOKEN="${NUMAPDE_GITLAB_TOKEN}" 'bash -s' < numapde-setup-ci-server.sh
```
on any system. It will
- install all required packages using the package manager
- install some python3 packages for the user `gitlab-runner`
- start the runner

### Runner registration per tag (or task?)
The runner requires a [registration](https://docs.gitlab.com/runner/register/index.html) per project and tag.  The command on **cicd.mathematik.tu-chemnitz.de**
```
PROJECTID=5315 # https://gitlab.hrz.tu-chemnitz.de/numapde-public/numapde-bibliography
TOKEN=$(curl --silent --header "Private-Token: ${NUMAPDE_GITLAB_TOKEN}" https://${NUMAPDE_GITLAB_SERVER}/api/v4/projects/${PROJECTID} | awk 'BEGIN {RS=","; FS="\""} /runners_token/ {print $4}')
ssh ${NUMAPDE_MRZ_LOGIN}@${NUMAPDE_GITLAB_CISERVER} sudo gitlab-runner register --non-interactive --limit 0 --request-concurrency 0 --url "https://${NUMAPDE_GITLAB_SERVER}" --executor "shell" --name "numapde-gitlab-runner" --registration-token "$TOKEN" --tag-list "canonicalize"
```
registers a runner with tag `canonicalize`. 


## Installation in docker
The installation in a docker container can be quite helpfull for testing the configuration before going to production. The  [gitlab documentation](https://docs.gitlab.com/runner/install/docker.html) describes the background and commands
- Build the container with

```
docker build -t "numapde-bibliography-tester" -f Dockerfile .
```

- It is advisable to store the configuration outside of the container to restore them after reboots or container restarts. Change to some folder outside the git repository. This folder will then store the runner configuration.
- Start the container in that folder.
```
docker run -d --name gitlab-runner-numapde --restart always \
    -v `pwd`/config:/etc/gitlab-runner \
    -v `pwd`/docker.sock:/var/run/docker.sock \
    numapde-bibliography-tester
```
- Start the runner:
```
docker exec -it gitlab-runner-numapde gitlab-runner start
```

- [Register](https://docs.gitlab.com/runner/register/index.html) the runner:
```
token=<GITLAB-RUNNER-TOKEN> ;  docker exec -it gitlab-runner-numapde gitlab-runner register --non-interactive --limit 0 --request-concurrency 0 --url "https://gitlab.hrz.tu-chemnitz.de/" --executor "shell" --name "numapde-gitlab-runner" --registration-token "$token" --tag-list "canonicalize"
```
- the GITLAB-RUNNER-TOKEN can be obtained [here](https://gitlab.hrz.tu-chemnitz.de/numapde-public/numapde-bibliography/-/settings/ci_cd)
