# numapde-bibliography

This repository provides the main bibliography `numapde.bib` for the numapde group. 
It includes a number of scripts in the folder `bin/` to amend and manipulate the bibliography file.
The scripts are written in `python` or for the shell (`bash`).

# Workflow
## Obtaining BibLaTeX entries
The `numapde.bib` file should **not be edited by hand**.
The following workflow describes the addition of entries in an automated way, by example.
To this end, we have a universal `numapde-get-bib.sh` retrieval script.
1. Query (as an example) arXiv for a publication by arXiv identifier: 
```bash
numapde-get-bib.sh 1709.01343
```
2. Check the outcome for obvious errors. 
If in doubt about the result, contact the maintainers. 
Notice that the citation key is temporary and will be set automatically when merging with an existing `.bib` file. 
3. If you are happy with the outcome, you may merge the query result into the target file, e.g., `numapde.bib`: 
```bash
numapde-get-bib.sh 1709.01343 --merge ~/Work/public/numapde-bibliography/numapde.bib
```
This will assign the final citation key, and call `numapde-merge-bibliography.py` to merge, avoiding duplicate entries.

In some cases, a query results in a slightly incorrect BibLaTeX code, e.g., `numapde-get-bib.sh 1902.07240` will be confused about the given and family names of the author `José Vidal Núñez` and will incorrectly report it as `Núñez, José Vidal` in the `AUTHOR` field.
In this case, use
```bash
numapde-get-bib.sh 1902.07240 --merge ~/Work/public/numapde-bibliography/numapde.bib --edit
```
to allow yourself to edit the results before the merge.

## Description of bibliography manipulation tools
In the following we describe all of our tools related to bibliography manipulation.
They will be listed here roughly in decreasing order of relevance to the average user.

### numapde-get-bib.sh
This is a universal citation retrieval script which currently supports queries to 
* https://arxiv.org
* https://www.crossref.org
* https://mathscinet.ams.org/mathscinet
The latter requires `mathscinet.pl` from https://github.com/gerw/mathbin, which is packaged as a submodule to `numapde-bibliography` but can also be obtained independently.
Notice that queries to [MathSciNet](https://mathscinet.ams.org/mathscinet) requires a subscription and may be available to you only through a VPN connection.

Notice that multiple queries can be passed in one go, such as
```bash
numapde-get-bib.sh -a herzog -t "superlinear+convergence" 1709.01343 1902.07240 10.1088/1361-6420/ab6d5b
```
See `numapde-get-bib.sh --help` for details.

### numapde-query-arxiv.sh, numapde-query-doi.sh, numapde-query-mathscinet.sh
These scripts are used internally by `numapde-get-bib.sh` but can be called also by the user.
They will specifically query 
* https://arxiv.org
* https://www.crossref.org
* https://mathscinet.ams.org/mathscinet
respectively, for the identifier or search pattern given.
Use `--help` to get more information.

### numapde-merge-bibliography.py
The call 
```
numapde-merge-bibliography.py new.bib old.bib
```
will merge the contents of `new.bib` into `old.bib`.
Duplicates will be recognized by way of
* arXiv identifier (in fact, the EPRINT field)
* DOI
and will be dropped.
`numapde-merge-bibliography.py` does not alter citation keys.

### numapde-generate-citekeys.py
The command
```
numapde-generate-citekeys.py new.bib
```
will generate temporary citation keys for the entries in `new.bib`.

### numapde-finalize-keys.sh

The call 
```
numapde-finalize-keys.sh numapde.bib
```
replace any temporary citation keys in `numapde.bib` by permanent ones, ensuring appropriate numbering.

### numapde-canonicalize-bibliography.sh
The command 

```
numapde-canonicalize-bibliography.sh numapde.bib
```
processes `numapde.bib` through various filters and
* sorts entries by citation key
* purges superfluous fields (such as MRREVIEWER)
* sorts fields alphabetically
* removes extra curly braces (not required by BibLaTeX

### numapde-validate-bibliography.sh
The command 
```
numapde-validate-bibliography.sh numapde.bib
```
performs a number of validation checks on `numapde.bib` including:
* duplicate citation keys
* citation keys not following the standard format
* extra curly braces
* abbreviated journal names
* sentence and title case

## Hints
* For easier access to your copy of `numapde.bib`, it may be useful to create a soft link to it in your home directory like
```bash
cd
ln -s ~/Work/public/numapde-bibliography/numapde.bib n.bib
```
so that you can say things like
```bash
numapde-get-bib.sh 1709.01343 --merge ~/n.bib
```
* If you add the directory containing `numapde.bib` to the environment variable `BIBINPUTS`,  you can say things like
```bash
numapde-get-bib.sh 1709.01343 --merge $(kpsewhich numapde.bib)
```

# Prerequisites
## Checking python requirements
The python scripts require
* python3 (lowest tested version: 3.6.9)
* pybtex (lowest tested version: 0.22.2)
```bash
for module in pybtex; do
  if ! pip3 list --format=columns | grep $module; then
    echo "$module MISSING"
    echo "Try: pip3 install $module"
  fi
done
```
## Checking bash requirements
```bash
for prog in awk biber cat curl diff gawk grep perl recode sed sponge wget xmllint; do 
  command -v $prog >/dev/null 2>&1 || echo "Command $prog is MISSING"; 
done 
```
### Installation hints
* `sponge` is part of [moreutils](https://joeyh.name/code/moreutils/) and is available as a linux package.
 
### Mac OS
On Mac OS you can use [Homebrew](https://brew.sh/) to install linux packages. 
For instance, for the package `moreutils`, say `brew install moreutils`.
